'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/guides/routing
|
*/

const Route = use('Route')

Route.on('/').render('landing')
Route
  .get('login', 'UserController.loginForm')
  .as('LoginForm')
Route.on('/register').render('register').as('RegisterForm')
Route.on('/privacy-policy').render('privacy-policy')
Route.on('/terms-of-use').render('terms-of-use').as('terms')
Route.on('/faq').render('faq')

Route
  .get('/blog/:slug', 'BlogController.show')

Route
  .get('/blog', 'BlogController.index')


Route
  .post('auth/login', 'UserController.login')
  .as('Login')

Route
  .post('auth/register', 'UserController.register')
  .as('Register')

Route
  .get('auth/confirm/:token', 'UserController.activate')
  .as('activateAccount')

Route
  .get('auth/logout', 'UserController.logout')
  .as('Logout')

Route
  .group(() => {
    Route
      .get('/dashboard', 'UserController.userDashboard')
      .as('UserDashboard')

    Route
      .post('/user/schedule', 'UserController.setUserSchedule')
      .as('userSchedule')

    Route
      .get('/explore-cuteness', 'UserController.upsell')
      .as('userUpsell')
  })
  .middleware(['auth', 'is:user'])

Route
  .group(() => {
    Route
      .get('dashboard', 'UserController.adminDashboard')
      .as('adminDashboard')
    Route
      .get('users/:id/edit', 'UserController.edit')
      .as('editUser')
    Route
      .put('users/:id/update', 'UserController.update')
      .as('userUpdate')
    Route
      .get('users/:id/delete', 'UserController.deleteUser')
      .as('userDelete')

    Route
      .get('logs', 'LogController.index')
      .as('logsIndex')

      Route
      .get('links', 'LinkController.index')
      .as('linksIndex')

    Route
      .get('links/:id/edit', 'LinkController.edit')
      .as('linkEdit')
    Route
      .put('links/:id/update', 'LinkController.update')
      .as('linkUpdate')
    Route
      .get('links/:id/delete', 'LinkController.deleteLink')
      .as('linkDelete')
  })
  .middleware(['auth', 'is:administrator'])
  .prefix('admin')


// Route.get('test-email', async function () {
//   const Env = use('Env')
//   const Mail = use('Mail')

//   try {
//     await Mail.send('emails.welcome', { name: 'adfasfa', confirmLink: 'adfsdfdasfadf' }, (message) => {
//       message
//         .to('petar@quantox.com')
//         .from(Env.get('FROM_EMAIL'), Env.get('FROM_EMAIL_NAME'))
//         .subject('Welcome to Awww')
//     })

//     return 'yay'
//   } catch (ex) {
//     return ex
//   }
// })
