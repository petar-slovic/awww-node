'use strict'

const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', table => {
      table.increments()
      table.string('name', 80).notNullable()
      table.string('email', 254).notNullable().unique()
      table.string('password', 60).notNullable()

      table.date('expirationDate')

      table.integer('lastVideoId')
      table.integer('lastImageId')

      table.integer('plan_id')

      table.string('ip')
      table.string('country')

      table.string('scheduleHours')
      table.string('scheduleMinutes')

      table.string('scheduleHoursOriginal')
      table.string('scheduleMinutesOriginal')

      table.boolean('activated')

      table.boolean('acceptedTerms')

      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema
