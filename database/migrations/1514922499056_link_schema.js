'use strict'

const Schema = use('Schema')

class LinkSchema extends Schema {
  up () {
    this.create('links', (table) => {
      table.increments()
      table.string('url').notNullable().unique()
      table.string('title')
      table.string('type').notNullable()
      table.string('animals')
      table.timestamps()
    })
  }

  down () {
    this.drop('links')
  }
}

module.exports = LinkSchema
