'use strict'

const Schema = use('Schema')

class PlanSchema extends Schema {
  up () {
    this.create('plans', (table) => {
      table.increments()
      table.timestamps()

      table.string('name').notNullable()
      table.string('description')
      table.decimal('price')
      table.boolean('active')

      /**
       * Duration in days
       * E.g. Free trial will last 7 days
       */
      table.integer('duration')
    })
  }

  down () {
    this.drop('plans')
  }
}

module.exports = PlanSchema
