'use strict'

const csv = require('csv-array')
const Helpers = use('Helpers')

/*
|--------------------------------------------------------------------------
| LinkSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

function getJsonFromCsv (url) {{
  return new Promise((resolve, reject) => {
    csv.parseCSV(url, data => {
      resolve(data)
    })
  })
}}

const Factory = use('Factory')
const Link = use('App/Models/Link')

class LinkSeeder {
  async run () {
    let linksPath = `${Helpers.resourcesPath()}/data`
    let links = await getJsonFromCsv(`${linksPath}/links.csv`)
    let linksAndjela = await getJsonFromCsv(`${linksPath}/links-andjela.csv`)

    let linksFormatted = links.map(link => ({
      url: link[Object.keys(link)[0]],
      type: link['Type'].toLowerCase(),
      animals: link[Object.keys(link)[3]]
    }))

    let linksAndjelaFormatted = linksAndjela.map(link => ({
      url: link[Object.keys(link)[0]],
      type: link['Type'].toLowerCase(),
      animals: link[Object.keys(link)[3]]
    }))

    Link.createMany(linksFormatted)
    Link.createMany(linksAndjelaFormatted)
  }
}

module.exports = LinkSeeder
