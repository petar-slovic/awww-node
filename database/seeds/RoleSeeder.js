'use strict'

/*
|--------------------------------------------------------------------------
| RoleSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')
const Role = use('Adonis/Acl/Role')

class RoleSeeder {
  async run () {
    const roleAdmin = new Role()
    roleAdmin.name = 'Administrator'
    roleAdmin.slug = 'administrator'
    roleAdmin.description = 'manage administration privileges'
    await roleAdmin.save()

    const roleUser = new Role()
    roleUser.name = 'User'
    roleUser.slug = 'user'
    roleUser.description = 'users the platform'
    await roleUser.save()
  }
}

module.exports = RoleSeeder
