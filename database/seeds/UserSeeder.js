'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const moment = require('moment')
const Factory = use('Factory')
const User = use('App/Models/User')
const Role = use('Adonis/Acl/Role')

class UserSeeder {
  async run () {
    const admin = await User
      .create({
        activated: true,
        name: 'Petar',
        email: 'petar@petar.io',
        password: 'petar',
        plan_id: 2,
        expirationDate: moment().add(1, 'year').format(),
        scheduleHours: 2,
        scheduleMinutes: 30
      })

    const adminRoleId = 1
    admin.roles().attach([adminRoleId])
    await admin.save()
  }
}

module.exports = UserSeeder
