'use strict'

/*
|--------------------------------------------------------------------------
| PlanSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory')
const Plan = use('App/Models/Plan')

class PlanSeeder {
  async run () {
    await Plan.createMany([{
      name: 'Kitty',
      description: 'One week Free Trial where you get the functionality of Tiger plan',
      price: 0,
      active: true,
      duration: 7
    }, {
      name: 'Tiger',
      description: 'Our most popular plan where you get 1 email / day with images and videos',
      price: 3.99,
      active: true,
      duration: 30
    }, {
      name: 'Lion',
      description: 'Premium plan where you get up to 10 emails / day with images and videos',
      price: 19.99,
      active: true,
      duration: 30
    }, {
      name: 'Special Tiger',
      description: 'Special free plan for the select few. Get the feautes of Tiger plan',
      price: 0,
      active: true,
      duration: 10000
    }])
  }
}

module.exports = PlanSeeder
