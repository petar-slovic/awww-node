'use strict'

const Encryption = use('Encryption')
const User = use('App/Models/User')
const Log = use('App/Models/Log')
const Plan = use('App/Models/Plan')
const Role = use('Adonis/Acl/Role')
const Database = use('Database')
const { validate } = use('Validator')
const Mail = use('Mail')
const Env = use('Env')
const moment = require('moment')

class UserController {
  async loginForm ({ view, auth, response }) {
    try {
      await auth.getUser()
      let roles = await auth.user.roles().fetch()

      const rolesJson = roles.toJSON()

      if (rolesJson.length && rolesJson[0].name === 'Administrator') {
        return response.route('adminDashboard')
      }

      return response.route('UserDashboard')
    } catch (error) {
      return view.render('login')
    }
  }

  async login({ request, auth, response, session }) {
    await auth.logout()

    const { email, password } = request.all()

    if (!email || !password) {
      session.flash({ notification: 'Wrong credentials.' })
      return response.route('LoginForm')
    }

    let user

    try {
      user = await User.findByOrFail('email', email)
    } catch (ex) {
      session.flash({ notification: 'Wrong credentials.' })
      return response.route('LoginForm')
    }

    if (!user.activated) {
      session.flash({ notification: 'Please activate the account by clicking the link in the welcome email.' })
      return response.route('LoginForm')
    }

    try {
      await auth.attempt(email, password)
    } catch (ex) {
      session.flash({ notification: 'There was a login error. Please contact us at petar@awww.ooo to resolve it!' })
      return response.route('LoginForm')
    }


    let roles = await auth.user.roles().fetch()

    const rolesJson = roles.toJSON()

    if (rolesJson.length && rolesJson[0].name === 'Administrator') {
      return response.route('adminDashboard')
    }

    response.route('UserDashboard')
  }

  async logout ({ auth, response }) {
    await auth.logout()

    response.route('LoginForm')
  }

  async register ({ request, response, session, auth }) {
    await auth.logout()

    const rules = {
      name: 'required',
      email: 'required|email|unique:users,email',
      password: 'required',
      country: 'required',
      terms: 'required'
    }
    const validation = await validate(request.all(), rules)

    if (validation.fails()) {
      session
      .withErrors(validation.messages())
      .flashExcept(['password'])
      return response.redirect('back')
    }

    let {
      name,
      email,
      password,
      country,
      plan_id,
      terms,
      utcOffset
    } = request.all()

    let user = await User.create({
      ip: request.ip(),
      name,
      email,
      password,
      country,
      acceptedTerms: !!terms
    })

    let hourOffset
    let minuteOffset

    try {
      hourOffset = parseInt(Number(utcOffset) / 60, 10)
      minuteOffset = Number(utcOffset) % 60
    } catch (ex) {
      session.flash({ notification: 'Validation failed.' })
      return response.redirect('back')
    }

    let hoursUtc = moment().utc().hours()
    let minutesUtc
    let min = moment().utc().minutes()

    if (min > 45) {
      minutesUtc = 0
    }

    if (min < 45) {
      minutesUtc = 45
    }

    if (min < 30) {
      minutesUtc = 30
    }

    if (min < 15) {
      minutesUtc = 15
    }

    user.scheduleHours = hoursUtc
    user.scheduleMinutes = minutesUtc

    user.scheduleHoursOriginal = Number(hoursUtc) - hourOffset
    user.scheduleMinutesOriginal = Number(minutesUtc) - minuteOffset

    let userRole = await Role.findByOrFail('name', 'User')
    user.roles().attach([userRole.id])

    await user.save()

    let token = await Encryption.encrypt(user.email)
    let confirmLink = `${Env.get('APP_DOMAIN')}/auth/confirm/${encodeURIComponent(token)}`

    Mail.send('emails.welcome', { name: user.name, confirmLink, absoluteUrl: Env.get('APP_DOMAIN') }, (message) => {
      message
        .to(user.email)
        .from(Env.get('FROM_EMAIL'), Env.get('FROM_EMAIL_NAME'))
        .subject('Welcome to Awww')
    })

    session.flash({ notification: `Please check your email for the activation link! If you don't see it in, please also check Spam.` })

    response.route('LoginForm')
  }

  async activate ({ session, params, response }) {
    let token = Encryption.decrypt(decodeURIComponent(params.token))

    try {
      let user = await User.findByOrFail('email', token)

      if (user.activated === 1) {
        session.flash({ notification: 'Acount already activated.' })
        return response.route('LoginForm')
      }

      user.activated = true

      let plan = await Plan.findByOrFail('name', 'Kitty')
      user.plan_id = plan.id
      user.expirationDate = plan.getExpirationDateFromNow().format()

      await user.save()
      session.flash({ notification: 'Account activated!' })
    } catch (ex) {
      session.flash({ notification: 'Dude... Invalid token.' })
    }
    response.route('LoginForm')
  }

  async adminDashboard ({ view }) {
    const users = await User
      .query()
      .with('plan')
      .fetch()

    const stats = await Database
      .select('*')
      .from('links')
      .groupBy('animals')
      .count('* as total')
      .orderBy('total', 'desc')
      .limit(4)
    return view.render('admin.dashboard', { users: users.toJSON(), stats })
  }

  async userDashboard ({ view, auth }) {
    const plan = await auth.user.plan().fetch()
    const hours = Array.from({ length: 23 }).map((_, x) => x < 10 ? `0${x}` : x + 1).map(x => x.toString())
    const minutes = Array.from({ length: 4 }).map((_, x) => x === 0 ? '00' : x * 15).map(x => x.toString())
    const expired = auth.user.isExpired()

    return view.render('user.dashboard', { plan: plan.toJSON(), hours, minutes, expired })
  }

  async edit({ params, response, view }) {
    let user = await User.find(params.id)
    let plans = await Plan.all()
    return view.render('admin.user-edit', { user: user.toJSON(), plans: plans.toJSON() })
  }

  async update({ params, request, response }) {
    const user = await User.find(params.id)

    user.name = request.input('name')
    user.email = request.input('email')
    user.country = request.input('country')
    user.ip = request.input('ip')

    user.activated = request.input('activated') === 'on'

    user.scheduleHours = request.input('scheduleHours')
    user.scheduleMinutes = request.input('scheduleMinutes')

    user.scheduleHoursOriginal = request.input('scheduleHours')
    user.scheduleMinutesOriginal = request.input('scheduleMinutes')

    if (!user.plan_id || (user.plan_id.toString() !== request.input('plan_id'))) {
      let plan = await Plan.findOrFail(request.input('plan_id'))
      user.expirationDate = plan.getExpirationDateFromNow().format()
    } else {
      user.expirationDate = request.input('expirationDate')
    }

    user.plan_id = request.input('plan_id')

    await user.save()

    response.route('adminDashboard')
  }

  async setUserSchedule ({ request, response, session, auth }) {
    const rules = {
      hours: 'required',
      minutes: 'required',
      utcOffset: 'required'
    }
    const validation = await validate(request.all(), rules)

    if (validation.fails()) {
      session.flash({ notification: 'Validation failed.' })
      return response.redirect('back')
    }

    let {
      hours,
      minutes,
      utcOffset
    } = request.all()

    let user = auth.user

    let hourOffset
    let minuteOffset

    try {
      hourOffset = parseInt(Number(utcOffset) / 60, 10)
      minuteOffset = Number(utcOffset) % 60
    } catch (ex) {
      session.flash({ notification: 'Validation failed.' })
      return response.redirect('back')
    }


    user.scheduleHours = Number(hours) + hourOffset
    user.scheduleMinutes = Number(minutes) + minuteOffset

    user.scheduleHoursOriginal = hours
    user.scheduleMinutesOriginal = minutes

    await user.save()

    session.flash({ notification: 'Schedule saved.' })

    response.route('UserDashboard')
  }

  async upsell ({ view, auth }) {
    const plan = await auth.user.plan().fetch()
    return view.render('user.upsell', { plan: plan.toJSON() })
  }

  async deleteUser ({ response, params }) {
    const { id } = params
    const user = await User.findOrFail(id)

    await Log
      .query()
      .where('user_id', id)
      .delete()

    await user.delete()

    return response.route('adminDashboard')
  }

}

module.exports = UserController
