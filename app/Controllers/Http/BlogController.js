'use strict'

const Helpers = use('Helpers')
const posts = require(`${Helpers.resourcesPath()}/data/posts`)

class BlogController {
  async index ({ view }) {
    return view.render('blog', { posts })
  }

  async show ({ view, params }) {
    const post = posts.find(post => post.slug === params.slug)
    return view.render('blog-post', { post, blogPartial: `blog.${post.slug}` })
  }
}

module.exports = BlogController
