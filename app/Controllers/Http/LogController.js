'use strict'

const Log = use('App/Models/Log')

class LogController {

  async index ({ view }) {
    const logs = await Log
      .query()
      .with('user')
      .fetch()

    return view.render('admin.logs', { logs: logs.toJSON() })
  }
}

module.exports = LogController
