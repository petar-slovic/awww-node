'use strict'

const Link = use('App/Models/Link')
const Database = use('Database')

class LinkController {
  async index ({ view }) {
    const links = await Link.all()
    const stats = await Database
      .select('type')
      .from('links')
      .groupBy('type')
      .count('type as total')
      .orderBy('type', 'asc')

    return view.render('admin.links', { links: links.toJSON(), stats })
  }

  async edit ({ view, params }) {
    const link = await Link.find(params.id)
    return view.render('admin.links-edit', { link: link.toJSON() })
  }

  async update ({ params, request, response }) {
    const link = await Link.find(params.id)
    link.title = request.input('title')
    link.type = request.input('type')
    link.animals = request.input('animals')
    link.url = request.input('url')

    await link.save()

    response.route('linksIndex')
  }

  async deleteLink ({ params, response }) {
    const link = await Link.find(params.id)
    await link.delete()
    response.route('linksIndex')
  }
}

module.exports = LinkController
