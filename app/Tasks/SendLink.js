'use strict'

const _ = require('lodash')
const moment = require('moment')

const User = use('App/Models/User')
const Link = use('App/Models/Link')
const Plan = use('App/Models/Plan')

const Mail = use('Mail')
const Env = use('Env')

class SendLink {

  static get schedule () {
    return Env.get('SEND_CRON_SCHEDULE')
  }

  async handle() {
    const users = await User.all()

    users.toJSON().forEach(async ({ id }) => {
      const user = await User.find(id)

      if (!user.activated) {
        console.log(moment().format('YYYY-MM-DD HH:mm:ss'), 'User not activated!', user.email)
        return
      }

      if (user.isExpired()) {
        console.log(moment().format('YYYY-MM-DD HH:mm:ss'), 'User expired!', user.email)
        return
      }

      if (!user.isScheduledForNow()) {
        console.log(moment().format('YYYY-MM-DD HH:mm:ss'), 'User is not Scheduled For Now!', user.email)
        return
      }

      const logs = await user
        .logs()
        .where('day', moment().format('YYYY-MM-DD'))
        .fetch()

      if (logs.toJSON().length) {
        console.log(moment().format('YYYY-MM-DD HH:mm:ss'), 'Already had a mail today!', user.email)
        return
      }

      const plan = await Plan.find(user.plan_id)

      let numberOfVideos = 0
      let numberOfImages = 0

      let videos = []
      let images = []

      switch (plan.name) {
        case 'Kitty':
          numberOfImages = 3
          numberOfVideos = 1
          break;

          case 'Tiger':
          numberOfImages = 3
          numberOfVideos = 1
          break;

        case 'Lion':
          numberOfImages = 15
          numberOfVideos = 5
          break;

        default:
          break;
      }

      if (numberOfVideos > 0) {
        videos = (await Link
          .query()
          .where('type', 'video')
          .andWhere('id', '>', user.lastVideoId || 0)
          .orderBy('id', 'asc')
          .limit(numberOfVideos)
          .fetch())
          .toJSON()
      }

      if (numberOfImages > 0) {
        images = (await Link
          .query()
          .where('type', 'image')
          .andWhere('id', '>', user.lastImageId || 0)
          .orderBy('id', 'asc')
          .limit(numberOfImages)
          .fetch())
          .toJSON()
      }

      if ((numberOfImages > 0 && images.length < numberOfImages) ||
        (numberOfVideos > 0 && videos.length < numberOfVideos)) {
        console.log(moment().format('YYYY-MM-DD HH:mm:ss'), 'No more cuteness for you', user.email)
        return
      }

      user.lastImageId = _.last(images).id
      user.lastVideoId = _.last(videos).id
      await user.save()

      await user.logs().create({
        links: [...images, ...videos].map(link => link.id).join(','),
        day: moment().format('YYYY-MM-DD')
      })

      let mailData = {
        images,
        videos,
        user,
        plan,
        absoluteUrl: Env.get('APP_DOMAIN')
      }

      console.log(moment().format('YYYY-MM-DD HH:mm:ss'), `Sending email to user`, user.email)

      Mail.send('emails.newsletter', mailData, (message) => {
        message
          .to(user.email)
          .from(Env.get('FROM_EMAIL'), Env.get('FROM_EMAIL_NAME'))
          .subject('Your cutenesses are here!')
      })
    })
  }

}

module.exports = SendLink
