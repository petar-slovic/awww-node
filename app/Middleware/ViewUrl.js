'use strict'

const Env = use('Env')

class ViewUrl {
  async handle ({ view }, next) {
    view.share({
      absoluteUrl: Env.get('APP_DOMAIN')
    })

    await next()
  }
}

module.exports = ViewUrl
