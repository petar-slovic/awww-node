'use strict'

const moment = require('moment')
const Model = use('Model')

class Plan extends Model {
  getExpirationDateFromNow () {
    return moment().add(this.duration, 'days')
  }
}

module.exports = Plan
