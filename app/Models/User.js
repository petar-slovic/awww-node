'use strict'

const moment = require('moment')
const Model = use('Model')

class User extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to bash the user password before saving
     * it to the database.
     *
     * Look at `app/Models/Hooks/User.js` file to
     * check the hashPassword method
     */
    this.addHook('beforeCreate', 'User.hashPassword')
  }

  static get traits() {
    return [
      '@provider:Adonis/Acl/HasRole'
    ]
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasMany('App/Models/Token')
  }

  plan () {
    return this.belongsTo('App/Models/Plan')
  }

  logs () {
    return this.hasMany('App/Models/Log')
  }

  isExpired () {
    return moment(this.expirationDate).isBefore(moment())
  }

  isScheduledForNow () {
    if (!this.scheduleMinutes || !this.scheduleHours) {
      return false
    }

    const bufferMinutes = 5

    let before5min = moment().utc().subtract(bufferMinutes, 'minutes')
    let after5min = moment().utc().add(bufferMinutes, 'minutes')

    let scheduledFor = moment().utc().hours(this.scheduleHours).minutes(this.scheduleMinutes)

    return scheduledFor.isBetween(before5min, after5min, 'minute', '[]')
  }

}

module.exports = User
